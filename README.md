# User Workload Monitoring Grafana

This repo configures a custom grafana instance (in the `custom-grafana`
namespace) with a datasource that has access to the `isitfriday` namespace
using the multi-tenant thanos-querier interface on OpenShift.

## Deploy

### When usingOpenShift GitOps

```bash
oc apply -f application.yaml
```

### Without OpenShift GitOps

Good luck. The following renders all manifests, but there is an inherent ordering between a lot of them, so you will need to apply them in the correct order. The ArgoCD annotations will help.

```bash
kustomize build . > big_manifest.yaml
```

